import { StyleSheet } from "react-native";
import { spacing } from "../../../components/config";
import { colorsTheme } from "../../../components/config/colors";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colorsTheme.white
    },
    departmentContainer: {
        paddingVertical: 10,
        width: "90%",
        backgroundColor: colorsTheme.white,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: 10,
        borderRadius: spacing.borderRadius.sm,
        paddingHorizontal: spacing.padding.base,
        alignSelf: "center"
    },
    titleDeparment: {
        color: colorsTheme.simiBlack,
        fontSize: spacing.fontSize.base
    },
    input: {
        width: "85%",
        alignSelf: 'center',
        borderRadius: spacing.borderRadius.base,
        borderColor: colorsTheme.gray, borderWidth: 1,
        borderBottomWidth: 1,
        borderBottomColor: colorsTheme.gray,
    },
    title: {
        color: colorsTheme.primary,
        marginVertical: spacing.margin.lg, width: "90%",
        alignSelf: 'center',
        fontWeight: "bold"
    },
    name: {
        color: colorsTheme.simiBlack,
        fontSize: spacing.fontSize.base,
    },
    nameBlue: {
        color: colorsTheme.primary,
        fontSize: spacing.fontSize.base,
        fontWeight: "bold",
        marginRight: 5
    },
    row: {
        flexDirection: "row",
        alignItems: "center"
    },
    btnSearch: {
        height: 44, width: 50,
        backgroundColor: colorsTheme.primary,
        borderRadius: spacing.borderRadius.base,
        right: 15
    }
});
export default styles