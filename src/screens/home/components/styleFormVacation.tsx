import { StyleSheet } from "react-native";
import { spacing } from "../../../components/config";
import { colorsTheme } from "../../../components/config/colors";

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: colorsTheme.white,
    },
    subTitle: {
        color: colorsTheme.gray,
        fontSize: spacing.fontSize.base,
    },
    btn: {
        width: '93%',
        marginVertical: 10,
    },
    ImgAvatar: {
        height: 300, width: "80%", resizeMode: "contain", alignSelf: "center"
    },
    containt: {
        width: '93%',
        flex: 1,
        alignSelf: "center",
        backgroundColor: colorsTheme.white
    },
    input: {
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    iconGlobal: {
        resizeMode: 'contain',
        height: 13,
        width: 13,
        marginLeft: 6,
    },
    title: {
        color: colorsTheme.gray,
        fontSize: spacing.fontSize.base,
        marginBottom: spacing.margin.xl,
    },
    txtPhone: {
        color: colorsTheme.danger,
        fontSize: spacing.fontSize.sm,
        paddingTop: spacing.margin.sm
    },
    departmentContainer: {
        paddingVertical: 10,
        width: "100%",
        backgroundColor: colorsTheme.white,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginVertical: 10,
        borderRadius: spacing.borderRadius.sm,
        paddingHorizontal: spacing.padding.base
    },
    titleDeparment: {
        color: colorsTheme.simiBlack,
        fontSize: spacing.fontSize.base
    },
    txtDepartment: {
        color: colorsTheme.primary,
        marginVertical: spacing.margin.base,
        fontWeight: "bold"
    }
});

export default styles