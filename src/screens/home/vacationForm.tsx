import React, { useState, useContext } from 'react';
import {
    View,
    Image,
    TouchableOpacity,
} from 'react-native';
import { FlatList } from 'react-native';
//@ts-ignore
import { useNavigation } from '@react-navigation/native';
import { colorsTheme, } from '../../components/config/colors';
import Text from '../../components/text';
import { homeStack } from '../../navigation/config';
import Button from '../../components/button';
import Input from '../../components/input';
import PickerDate from '../../components/pickerDate';
import AsyncStorage from '@react-native-community/async-storage';
import vacationContext from '../../context/vacationContext';
import styles from './components/styleFormVacation';

const VacationForm = () => {
    const navigation = useNavigation();
    const { vacationData, setVacationData } = useContext(vacationContext);
    const [name, setName] = useState('');
    const [isValidName, setNameValidation] = useState(true);
    const [mobile, setMobile] = useState(null || '');
    const [requestedDays, setRequestedDays] = useState(null || '');
    const [isValidRequestedDays, setRequestedDaysValidation] = useState(true);
    const [date, setDate] = useState("");
    const [isValidDate, setDateValidation] = useState(true);
    const [departmentName, setDepartmentName] = useState(0);
    const [isValidDepartmentName, setDepartmentNameValidation] = useState(true);
    const DepartmentData = [
        { id: 1, departmentName: "department_1" },
        { id: 2, departmentName: "department_2" },
        { id: 3, departmentName: "department_3" },
    ]
    const VacationFormReq = async () => {
        let newValues = {}
        const values: any = {
            name: name,
            mobile: mobile,
            departmentName: departmentName,
            date: date,
            requestedDays: requestedDays
        };
        console.log(values, "==========");

        if (name === '') {
            setNameValidation(false);
        }
        else if (requestedDays === null || requestedDays === '') {
            setRequestedDaysValidation(false);
        }
        else if (date === "") {
            setDateValidation(false);
        }
        else if (departmentName === 0) {
            setDepartmentNameValidation(false);
        }
        else {
            try {
                const savedArr = await AsyncStorage.getItem('vacationData')
                let _savedArr;
                if (!savedArr) _savedArr = []
                else _savedArr = JSON.parse(savedArr)
                const newArray = [..._savedArr, values]
                await AsyncStorage.setItem(
                    'vacationData',
                    JSON.stringify(newArray)
                );
                //@ts-ignore
                setVacationData(newArray);
                navigation.navigate(homeStack.vacation)
            } catch (error) {
            }
        }
    }

    const headerComponent = () => {
        return (
            <View style={styles.scroll}>
                <View>
                    <View style={[styles.row]}>
                        <Input
                            isRequired
                            value={name}
                            title={'name'}
                            onChange={(value: any) => {
                                setName(value)
                                setNameValidation(true)
                            }}
                            placeHolder={'Enter Name'}
                            style={[styles.input, {
                                borderBottomColor: isValidName ?
                                    colorsTheme.borderGray : colorsTheme.danger
                            }]}
                        />
                    </View>
                    {isValidName === false ? <Text title={'please enter name'} style={styles.txtPhone} /> : null}
                </View>
                <View>
                    <View style={[styles.row]}>
                        <Input
                            value={mobile}
                            keyboardType="numeric"
                            title={'mobile'}
                            onChange={(value: any) => {
                                setMobile(value)
                            }}
                            placeHolder={'Enter Mobile Number'}
                            style={[styles.input, { borderBottomColor: colorsTheme.borderGray }]}
                        />
                    </View>
                </View>
                <View>
                    <View style={[styles.row]}>
                        <Input
                            isRequired
                            value={requestedDays}
                            keyboardType="numeric"
                            title={'Number Requested Days'}
                            onChange={(value: any) => {
                                setRequestedDays(value)
                                setRequestedDaysValidation(true)
                            }}
                            placeHolder={'Enter Number Requested Dayes'}
                            style={[styles.input, {
                                borderBottomColor: isValidRequestedDays ?
                                    colorsTheme.borderGray : colorsTheme.danger
                            }]}
                        />
                    </View>
                    {isValidRequestedDays === false ? <Text title={'Please Enter Number Requested Days'} style={styles.txtPhone} /> : null}
                </View>
                <View>
                    <PickerDate
                        isRequired
                        datePickerState={date}
                        datePickerSetState={(value: any) => {
                            setDate(value)
                            setDateValidation(true)
                        }}
                        title={"Start Date"}
                    />
                    {isValidDate === false ? <Text title={'please Enter date'} style={styles.txtPhone} /> : null}
                </View>
                <View style={styles.row}>
                    <Text title={"choose department"} style={[styles.txtDepartment]} />
                    <Text title={"*"} style={[styles.txtDepartment, { marginLeft: 3 }]} />
                </View>
            </View>
        )
    }
    return (
        <View style={styles.scroll}>
            <Image
                source={require('../../assets/images/vacation.jpg')}
                style={styles.ImgAvatar}
            />
            <FlatList
                keyExtractor={(item: any, index: any) => index.toString()}
                style={styles.containt}
                showsVerticalScrollIndicator={false}
                ListHeaderComponent={headerComponent()}
                ListHeaderComponentStyle={styles.scroll}
                data={DepartmentData}
                renderItem={({ item, index }: any) => (
                    <>
                        <TouchableOpacity
                            key={item?.id}
                            onPress={() => {
                                setDepartmentName(item.departmentName);
                                setDepartmentNameValidation(true)
                            }}
                            style={[
                                styles.departmentContainer,
                                {
                                    borderColor: departmentName === item.departmentName ?
                                        colorsTheme.primary : colorsTheme.secondryGray,
                                    borderWidth: 1
                                },
                            ]}>
                            <Text title={item.departmentName} txtStyle={[
                                styles.titleDeparment,
                                {
                                    color: departmentName === item.departmentName ?
                                        colorsTheme.simiBlack : colorsTheme.gray,
                                },
                            ]} />
                            <Image source={departmentName === item.departmentName ?
                                require("../../assets/images/check.png") : require("../../assets/images/checkBox-inactive.png")} />
                        </TouchableOpacity>
                    </>
                )}
            />
            {isValidDepartmentName === false ? <Text title={'please Enter department name'} style={styles.txtPhone} /> : null}

            <Button
                title={'Show All Vacation'}
                style={styles.btn}
                txtStyle={{ color: colorsTheme.white }}
                onPress={() => {
                    console.log('department form');
                    VacationFormReq()
                }}
            />
        </View>
    );
};
export default VacationForm