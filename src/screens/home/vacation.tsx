import AsyncStorage from '@react-native-community/async-storage';
import * as React from 'react';
import { useContext } from 'react';
import { View, FlatList, TouchableOpacity, Image } from 'react-native';
import Button from '../../components/button';
import { spacing } from '../../components/config';
import { colorsTheme } from '../../components/config/colors';
import Header from '../../components/header';
import Input from '../../components/input';
import Text from '../../components/text';
import vacationContext from '../../context/vacationContext';
import styles from './components/styleVacation';

const Vacation = () => {
    const { vacationData, setVacationData } = useContext(vacationContext);
    const [name, setName] = React.useState('');
    const [newArray, setNewArray] = React.useState([]);

    const remove = async (vacation: String) => {
        //@ts-ignore
        const vacationDeleted = vacationData.filter(d => d?.name !== vacation);
        console.log(vacationDeleted, "filtered");
        await AsyncStorage.setItem(
            'vacationData',
            JSON.stringify(vacationDeleted)
        );
        //@ts-ignore
        setVacationData(vacationDeleted)
        return vacationDeleted
    }
    const refresh = () => {
        const array = vacationData.filter(data => {
            //@ts-ignore
            if (data?.name === name) {
                //@ts-ignore
                setNewArray([data])
                // console.log(newArray, "newArray-----");
            } else {
                console.log("no ky in search");
            }
        });
    }
    return (
        <View style={styles.container}>
            <Header title={'List Vacation'} />
            {vacationData.length !== 0 ?
                <>
                    <View style={{ alignSelf: "center" }}>
                        <View style={[styles.row, { width: "90%", marginTop: spacing.margin.base }]}>
                            <Input
                                value={name}
                                onChange={(value: any) => {
                                    setName(value)
                                    refresh()
                                }}
                                placeHolder={"Search"}

                                style={styles.input}
                            />
                            <Button
                                type={"primary"}
                                title={"search"}
                                onPress={() => {
                                    refresh()
                                }}
                                style={styles.btnSearch}
                                txtStyle={{ color: colorsTheme.white, fontSize: 10 }}
                            />
                        </View>
                        <Text title={"List Vacation"} style={styles.title} />
                    </View>
                    <FlatList
                        keyExtractor={(item: any, index: any) => index.toString()}
                        style={{ flex: 1 }}
                        data={name === '' ? vacationData : newArray}
                        renderItem={({ item, index }: any) => (
                            <>
                                <View
                                    key={index.toString()}
                                    style={[
                                        styles.departmentContainer,
                                        {
                                            borderColor: colorsTheme.primary,
                                            borderWidth: 1
                                        },
                                    ]}>
                                    <View style={{ paddingVertical: spacing.padding.sm }}>
                                        <View style={[styles.row, { maxWidth: "90%" }]}>
                                            <Text title={"Name :"} style={[styles.nameBlue]} />
                                            <Text title={item.name} style={[styles.name]} />
                                        </View>

                                        <View style={[styles.row, {
                                            paddingTop: spacing.padding.sm,
                                        }]}>
                                            <Text title={"Department :"} style={[styles.nameBlue]} />
                                            <Text title={item.departmentName} style={[
                                                styles.titleDeparment,
                                                {
                                                    color: colorsTheme.simiBlack,
                                                },
                                            ]} />
                                        </View>

                                        <View style={[styles.row, {
                                            paddingTop: spacing.padding.sm,
                                        }]}>
                                            <Text title={"Number Requested Days :"} style={[styles.nameBlue]} />
                                            <Text title={item.requestedDays} style={[styles.name]} />
                                        </View>
                                        <View style={[styles.row, {
                                            paddingTop: spacing.padding.sm,
                                        }]}>
                                            <Text title={"start Date :"} style={[styles.nameBlue]} />
                                            <Text title={item.date} style={[styles.name]} />
                                        </View>
                                    </View>
                                    <TouchableOpacity activeOpacity={.9} onPress={() => {
                                        remove(item?.name);
                                        console.log("remove");
                                    }}>
                                        <Image source={require("../../assets/images/trash-green.png")}
                                            style={{
                                                height: 22, width: 22, resizeMode: "contain",
                                                alignSelf: "center"
                                            }}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </>
                        )}
                    />
                </> :
                <View style={{ justifyContent: "center", alignSelf: "center", alignItems: "center", flex: 1 }}>
                    <Image source={require("../../assets/images/no-vacation.jpg")} style={{
                        height: 400, width: 400,
                        marginHorizontal: spacing.margin.base,
                        resizeMode: "contain",
                    }} />
                    <Text title={"you don't have any vacation"} style={[styles.title, { marginVertical: 5 }]} />
                </View>
            }
        </View>
    );
};

export default Vacation;
