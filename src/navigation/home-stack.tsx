import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import VacationForm from '../screens/home/vacationForm';
import { homeStack } from './config';
import Vacation from '../screens/home/vacation';

const Stack = createStackNavigator();
export default ({ name }: any) => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name={homeStack.vacationForm} component={VacationForm} />
            <Stack.Screen name={homeStack.vacation} component={Vacation} />
        </Stack.Navigator>
    );
};
