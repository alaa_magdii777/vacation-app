import * as React from 'react';
import SplashScreen from 'react-native-splash-screen';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import homeStack from './home-stack';
import { rootSwitch } from './config';
const Stack = createStackNavigator();

const AppNavigator = (props: any) => {
    SplashScreen.hide();
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{ headerShown: false }}>
                <Stack.Screen name={rootSwitch.homeStack} component={homeStack} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};
export default AppNavigator;
