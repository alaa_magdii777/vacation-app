import React, { useState } from 'react';

const VacationContext = React.createContext({
  vacationData: [{}],
  setVacationData: () => [{}],
});

export default VacationContext;

