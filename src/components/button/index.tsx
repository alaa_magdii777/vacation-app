import {
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import React from 'react';
import { fonts, spacing } from '../config';
import { colorsTheme, } from '../config/colors';
const Button = ({
  title,
  onPress,
  style = [],
  txtStyle = [],
  loading,
  icon,
  disabled,
}: any) => {
  const { white }: any = colorsTheme;
  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={loading || disabled}
      style={[
        styles.Btn,
        style,
      ]}>
      {loading ? (
        <ActivityIndicator color={white} />
      ) : icon ? (
        <Image source={icon} style={{ resizeMode: 'contain' }} />
      ) :
        (
          <Text
            style={[
              styles.TxtBtn,
              txtStyle,
            ]}>
            {title}
          </Text>
        )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  Btn: {
    height: 48,
    width: 167,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    marginVertical: 10,
    borderRadius: spacing.borderRadius.sm,
    backgroundColor: colorsTheme.primary
  },
  TxtBtn: {
    fontSize: spacing.fontSize.base,
    fontFamily: fonts.simiBold.fontFamily,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    color: colorsTheme.simiBlack
  },
});

export default Button;
