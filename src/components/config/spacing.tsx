// Border radius
export const borderRadius = {
    base: 12,
    sm: 8,
    md: 41,
    lg: 42,
};

// Line height
export const lineHeight = {
    base: 20,
    small: 8,
    large: 16,
    big: 24,
};

// Padding
export const padding = {
    base: 16,
    sm: 8,
    lg: 20,
    xl: 24,
};

// Margin
export const margin = {
    base: 16,
    sm: 8,
    lg: 20,
    xl: 24,
};
const fontSize = {
    base: 14,
    sm: 12,
    lg: 16,
    xl: 20,
};
export default {
    borderRadius,
    lineHeight,
    padding,
    margin,
    fontSize
};
