import { I18nManager } from "react-native";

const fontTheme_ar = {
    Family: {
        regular: 'Cairo-Regular',
        simiBold: 'Cairo-SemiBold',
        bold: "Cairo-Bold",
    },
};

const fontTheme_en = {
    Family: {
        regular: 'Cairo-Regular',
        simiBold: 'Cairo-SemiBold',
        bold: "Cairo-Bold",
    },
};

const Font = I18nManager.isRTL ? fontTheme_ar : fontTheme_en;

// Export font size
export const sizes = {
    base: 14,
    h1: 30,
    h2: 24,
    h3: 20,
    h4: 16,
    h5: 14,
    h6: 12,
};

// Export lineheights
export const lineHeights = {
    base: 20,
    h1: 43,
    h2: 33,
    h3: 28,
    h4: 23,
    h5: 20,
    h6: 17,
};

// Export font family
export default {
    regular: {
        fontFamily: Font.Family.regular,
    },

    simiBold: {
        fontFamily: Font.Family.simiBold,
    },

    bold: {
        fontFamily: Font.Family.bold,
        fontWeight: 'bold',
    },

    default: {},
};
