export const white = '#ffffff';
export const grey1 = '#f4f4f4';
export const grey2 = '#e9ecef';
export const grey3 = '#dee2e6';
export const grey4 = '#adb5bd';
export const grey5 = '#999999';
export const grey6 = '#777777';
export const grey7 = '#383838';
export const grey8 = '#1e1e1e';
export const grey9 = '#2C2C2C';
export const simiBlack = '#121212';

export const colorsTheme = {
  lightBg: " rgba(132, 188, 164, 0.02)",
  primary: '#1985A1',
  txtBlue: "#1985A1",
  Secondary: "#54A5DA",
  danger: '#F9663A',
  red: "#EB5757",
  lightRed: "rgba(228, 74, 61, 0.64)",
  orange: "#F2994A",
  warning: "#FCBA10",
  purpel: "#5D5FEF",
  silver: "#7C7C7C",
  gray: '#747474',
  success: "#42A77A",
  white: 'white',
  grayDark: "#4C5673",
  simiBlack: '#37474F',
  secondaryBlack: '#181725',
  borderBlue: 'rgba(45, 156, 219, 0.6)',
  borderOrange: 'rgba(242, 153, 74, 0.6)',
  lightGray: '#FCFCFC',
  link: '#27A4FF',
  secondryGray: '#E1E1E1',
  differentGray: '#828282',
  thirdGray: 'rgba(22, 89, 62, 0.08)',
  fourGray: '#F2F2F2',
  fiveGray: 'rgba(22, 89, 62, 0.04)',
  transparent: 'transparent',
  divider: '#E1E1E1',
  black: '#000000',
  simiGreen: 'rgba(22, 89, 62, 0.4)',
  lightGreen: 'rgba(22, 89, 62, 0.12)',
  borderGray: '#E2E2E2',
  anotherGray: '#97ADB6',
  borderBoxShadow: 'rgba(55, 71, 79, 0.12)',
  light: '#F8F8F8',
  sixGray: 'rgba(0, 0, 0, 0.56)',
  sevenGray: '#BDBDBD',
  eightGray: 'rgba(55, 71, 79, 0.08)',
  lightBlue: 'rgba(66, 167, 122, 0.08)',
  error: 'rgba(228, 74, 61, 0.08)',
  whiteShadow: 'rgba(22, 89, 62, 0.12)',
  opacity: 'transparent',
  gradientOne: '#53B175',
  gradientTwo: '#16593E',
  grayLine: '#D1DBD6',
  anotherSimiGreen: 'rgba(22, 89, 62, 0.2)',
  grayTitle: '#7C7C7C',
  anotherBlack: '#030303',
  lightLight: 'rgba(55, 71, 79, 0.16)',
  lightUpload: "rgba(55, 71, 79, 0.06)",
  bgGray: "rgba(55, 71, 79, 0.04)",
  borderBox: "#AABBC6",
  ViewLabel: {
    color: grey5,
    colorHeading: simiBlack,
  },
};
