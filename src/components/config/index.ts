import { getStatusBarHeight } from 'react-native-status-bar-height';

// import colors, {lightColors, darkColors} from './colors';
import fonts, { sizes, lineHeights } from './fonts';
import spacing from './spacing';

export {
  //   colors,
  //   lightColors,
  //   darkColors,
  getStatusBarHeight,
  fonts,
  sizes,
  lineHeights,
  spacing,
};
