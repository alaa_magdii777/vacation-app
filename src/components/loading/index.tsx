import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

export default function Loading({ color }: any) {
    return (
        <View style={styles.Indicator}>
            <ActivityIndicator size="small" color={color}
                style={{ justifyContent: "center", marginTop: 100 }}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    Indicator: {
        flex: 1,
    },
});
