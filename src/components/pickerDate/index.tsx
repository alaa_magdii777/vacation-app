import * as React from 'react';
import { StyleSheet, Image, View } from 'react-native';
//@ts-ignore
import DatePicker from 'react-native-datepicker';
import { spacing } from '../config';
import { colorsTheme } from '../config/colors';
import Text from '../text';

interface PickerDateProps {
    datePickerState?: any;
    datePickerSetState?: any;
    title?: string | any;
    titleStyle?: any
    isRequired?: any
}

const PickerDate = (props: PickerDateProps) => {
    return (
        <View style={{ marginBottom: spacing.margin.base }}>
            {props.isRequired ?
                <View style={styles.row}>
                    <Text title={props.title} style={[props.titleStyle, styles.title]} />
                    <Text title={'*'} style={[props.titleStyle, styles.title, { marginLeft: 3 }]} />
                </View> :
                <Text title={props.title} style={[props.titleStyle, { marginLeft: 13 }]} />
            }
            <DatePicker
                style={styles.pickerDate}
                date={props.datePickerState}
                mode="date"
                placeholder="Start date"
                format="YYYY-MM-DD"
                minDate="2021-01-01"
                maxDate="2022-08-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"

                iconComponent={
                    <Image
                        style={styles.icon}
                        source={require('../../assets/images/datePicker.png')} />
                }
                customStyles={{
                    dateInput: {
                        alignItems: "flex-start",
                        paddingLeft: 10,
                        fontSize: 10, borderBottomWidth: .5,
                        color: '#101010', borderRadius: 5,
                        borderTopWidth: 0,
                        borderRightWidth: 0,
                        borderLeftWidth: 0,
                    }
                }}
                onDateChange={props.datePickerSetState}
            />
        </View>
    );
};

export default PickerDate;

const styles = StyleSheet.create({
    pickerDate: {
        width: '100%',
        height: 40,
        alignItems: "flex-end",
        backgroundColor: "white",
        alignSelf: 'center',
    },
    icon: {
        height: 20,
        width: 20,
        position: "absolute",
        right: 1,
        marginHorizontal: 10
    },
    row: {
        flexDirection: "row",
        alignItems: "center"
    },
    title: {
        fontSize: spacing.fontSize.base,
        color: colorsTheme.txtBlue,
        marginTop: 32,
        marginBottom: 10,
        fontWeight: "bold"
    }
});
