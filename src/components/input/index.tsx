import * as React from 'react';
import { View, StyleSheet, TextInput, } from 'react-native';
import { fonts, spacing } from '../config';
import { colorsTheme } from '../config/colors';
import Text from '../text';

const Input = ({
    title,
    onChange,
    style,
    keyboardType,
    placeHolder,
    placeholderTextColor,
    titleStyle,
    value,
    isRequired
}: any) => {
    return (
        <View style={styles.container}>
            {isRequired ?
                <View style={styles.row}>
                    <Text title={title} style={[styles.title, titleStyle]} />
                    <Text title={"*"} style={[styles.title, titleStyle, { marginLeft: 3 }]} />
                </View>
                : title ?
                    <Text title={title} style={[styles.title, titleStyle]} /> : null
            }
            <View style={[styles.containerInput, style]}>
                <TextInput
                    keyboardType={keyboardType}
                    placeholder={placeHolder}
                    placeholderTextColor={
                        !placeholderTextColor ? colorsTheme.grayTitle : placeholderTextColor
                    }
                    value={value}
                    onChangeText={onChange}
                    style={[styles.input,]}
                />
            </View>
        </View>
    );
};

export default Input;

const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    title: {
        fontSize: spacing.fontSize.base,
        color: colorsTheme.txtBlue,
        marginTop: 30,
        marginBottom: 10,
        fontWeight: "bold"
    },
    containerInput: {
        borderBottomWidth: 1,
        borderBottomColor: colorsTheme.borderGray,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: 44,
        marginHorizontal: spacing.margin.base,
        alignSelf: 'center',
    },
    input: {
        textAlign: "left",
        color: colorsTheme.anotherBlack,
        fontFamily: fonts.regular.fontFamily,
        width: '100%',
        fontSize: spacing.fontSize.base
    },
    row: {
        flexDirection: "row",
        alignItems: "center"
    }
});
