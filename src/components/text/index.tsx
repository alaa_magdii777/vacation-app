import React from 'react';
import { StyleSheet, Text as TextComponent } from 'react-native';
import { fonts, spacing } from '../config';
import { colorsTheme } from '../config/colors';

export default function Text({ title, style = [], numberOfLines }: any) {
    return (
        <TextComponent
            numberOfLines={numberOfLines}
            style={[styles.default, style]}>
            {title}
        </TextComponent>
    );
}

const styles = StyleSheet.create({
    default: {
        fontSize: spacing.fontSize.lg,
        fontFamily: fonts.simiBold.fontFamily,
        color: colorsTheme.simiBlack,
    },
});
