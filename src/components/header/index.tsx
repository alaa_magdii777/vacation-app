import { useTheme } from '@react-navigation/native';
import * as React from 'react';
import { View, StyleSheet, Platform, TouchableOpacity, Image } from 'react-native';
import { fonts, getStatusBarHeight, spacing } from '../config';
import { colorsTheme } from '../config/colors';
import Text from '../text';
import { useNavigation } from '@react-navigation/native';

const Header = ({
    firstElement,
    title,
    showDivider,
    style,
}: any) => {
    const navigation = useNavigation();
    return (
        <View
            style={[
                styles.container,
                style,
                {
                    borderBottomWidth: showDivider ? 1 : 0,
                    borderBottomColor: showDivider ? colorsTheme.divider : colorsTheme.white,
                },
            ]}>
            <View style={styles.containerHeader}>
                {firstElement ? <View>{firstElement}</View> : (
                    <>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <Image source={require("../../assets/images/backLeft-white.png")} style={{ resizeMode: "contain", height: 40, width: 40 }} />
                        </TouchableOpacity>
                        <Text title={title} style={styles.title} />
                    </>
                )}
            </View>
        </View>
    );
};

export default Header;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colorsTheme.primary,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        width: '100%',
        marginBottom: 1,
    },
    containerHeader: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        paddingHorizontal: spacing.padding.sm,
        alignContent: 'center',
        paddingTop: getStatusBarHeight(),
        height:
            Platform.select({
                android: 56,
                default: 56,
            }) + getStatusBarHeight(),
    },
    title: {
        fontSize: 18,
        fontFamily: fonts.bold.fontFamily,
        color: colorsTheme.white,
        marginHorizontal: 5,
    },
});
