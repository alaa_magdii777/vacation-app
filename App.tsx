import * as React from 'react';
import { StyleSheet, StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AppNavigator from './src/navigation/root-switch';
import Toast from 'react-native-toast-message';
import { useState } from 'react';
import VacationContext from './src/context/vacationContext';
import { colorsTheme } from './src/components/config/colors';
import AsyncStorage from '@react-native-community/async-storage';
const App = () => {
  const barStyle = 'dark-content';
  const [vacationData, setVacationData] = useState({});
  const VacationContextValue = React.useMemo(() => ({ vacationData, setVacationData }), [vacationData]);
  React.useEffect(() => {
    AsyncStorage.getItem("vacationData").then((value: any) => {
      JSON.parse(value)
    })
  }, []);
  return (
    //@ts-ignore
    <VacationContext.Provider value={VacationContextValue}>
      <StatusBar translucent barStyle={barStyle} backgroundColor={colorsTheme.primary} />
      <SafeAreaProvider style={styles.container}>
        <AppNavigator />
        <Toast ref={(ref: any) => Toast.setRef(ref)} />
      </SafeAreaProvider>
    </VacationContext.Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
